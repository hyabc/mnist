#include <cstdio>
#include "read.h"
inline unsigned int getint() {
	return ((unsigned int)(getchar()) << 24) + ((unsigned int)(getchar()) << 16) + ((unsigned int)(getchar()) << 8) + (unsigned int)(getchar());
}
void Training_set_label::read() {
	freopen("train-labels.idx1-ubyte", "rb", stdin);
	magic_number = getint();
	size = getint();
	for (int i = 0;i < size;i++) value[i] = getchar();
	fclose(stdin);
}

void Training_set_image::read() {
	freopen("train-images.idx3-ubyte", "rb", stdin);
	magic_number = getint();
	size = getint();
	row = getint();
	col = getint();
	for (int i = 0;i < size;i++) for (int j = 0;j < row;j++) for (int k = 0;k < col;k++) value[i][j * col + k] = getchar();
	fclose(stdin);
}
void Test_set_label::read() {
	freopen("t10k-labels.idx1-ubyte", "rb", stdin);
	magic_number = getint();
	size = getint();
	for (int i = 0;i < size;i++) value[i] = getchar();
	fclose(stdin);
}
void Test_set_image::read() {
	freopen("t10k-images.idx3-ubyte", "rb", stdin);
	magic_number = getint();
	size = getint();
	row = getint();
	col = getint();
	for (int i = 0;i < size;i++) for (int j = 0;j < row;j++) for (int k = 0;k < col;k++) value[i][j * col + k] = getchar();
	fclose(stdin);
}
