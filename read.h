#ifndef READ_H
#define READ_H
unsigned int getint();
struct Training_set_label {
	unsigned int magic_number, size;
	unsigned int value[60010];
	void read();
};
struct Training_set_image {
	unsigned int magic_number, size, row, col;
	unsigned int value[60010][800];
	void read();
};
struct Test_set_label {
	unsigned int magic_number, size;
	unsigned int value[10010];
	void read();
};
struct Test_set_image {
	unsigned int magic_number, size, row, col;
	unsigned int value[10010][800];
	void read();
};
#endif
